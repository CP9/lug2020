﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;


namespace LUG_CLASE1
{
    class Acceso
    {
        private SqlConnection conexion;

        public bool Abrir()
        {
            bool ok;
            if (conexion != null && conexion.State == System.Data.ConnectionState.Open)
            {
                ok = false;
            }
            else
            {
                conexion = new SqlConnection();

                try
                {
                    conexion.ConnectionString = @"Data Source=.\SqlExpress; Initial Catalog=LUG; Integrated Security=SSPI";

                    conexion.Open();
                    ok = true;
                }
                catch (SqlException ex)
                {
                    ok = false;
                }

            }
            return ok;
        }
        public void Cerrar()
        {
            if (conexion != null)
            { 
                conexion.Close();
                conexion.Dispose();
                conexion = null;
                GC.Collect();
            }
        }


        public int Escribir(string SQL, List<SqlParameter> parametros = null)
        {
            int filasAfectadas;

            SqlCommand comando = new SqlCommand();

            comando.Connection = conexion;

            comando.CommandText = SQL;

            comando.CommandType = System.Data.CommandType.Text;

            if (parametros != null && parametros.Count > 0)
            {

                #region "variantes de agregado de parámetros"
                /*  foreach (SqlParameter p in parametros)
                  {
                      comando.Parameters.Add(p);
                  }
                 
                foreach (SqlParameter p in parametros)
               {
                   comando.Parameters.AddWithValue (p.ParameterName,p.Value);
               }
               */
                #endregion

                comando.Parameters.AddRange(parametros.ToArray());

              
            }


            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }

            catch (SqlException ex)
            {
                filasAfectadas = -1;
            }
            catch (Exception ex)
            {
                filasAfectadas = -2;
            }
            return filasAfectadas;
        }


        public SqlDataReader Leer(string SQL)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = SQL;
            comando.CommandType = System.Data.CommandType.Text;

            SqlDataReader lector = comando.ExecuteReader();

            return lector;

        }

        public int LeerEscalar(string SQL)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = SQL;
            comando.CommandType = System.Data.CommandType.Text;
            int resultado = int.Parse( comando.ExecuteScalar().ToString())   ;

            return resultado;

        }



    }
}
