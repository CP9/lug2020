﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;
namespace Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Persona.Listar();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Persona p = new Persona();
            p.Nombre = textBox2.Text;

            p.Insertar();

            Enlazar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Persona p = new Persona(0, textBox2.Text);
         

            Enlazar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Persona p = new Persona(int.Parse(textBox1.Text), textBox2.Text);
            p.Editar();

            Enlazar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Persona p = new Persona(int.Parse(textBox1.Text), textBox2.Text);
            p.Borrar();

            Enlazar();
        }
    }
}
