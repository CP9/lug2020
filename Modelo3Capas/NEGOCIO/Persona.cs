﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;

namespace NEGOCIO
{
    public class Persona
    {
        public Persona() { }

        public Persona(int I, string n ) {


            id = I;
            nombre = n;
            if(id == 0)
            { 
                Insertar();
            }
        }


        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private Acceso acceso = new Acceso(); 

        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));
            acceso.Escribir("PERSONA_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar()
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@id", this.id));
            acceso.Escribir("PERSONA_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                  
            parameters.Add(acceso.CrearParametro("@id", this.id));
            acceso.Escribir("PERSONA_BORRAR", parameters);

            acceso.Cerrar();
        }

        public static List<Persona> Listar()
        {
            List<Persona> lista = new List<Persona>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("PERSONA_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Persona p = new Persona();
                p.nombre = registro[1].ToString();
                p.id = int.Parse(registro[0].ToString());
                lista.Add(p);
            }
            return lista;
        }


    }
}
