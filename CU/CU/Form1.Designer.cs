﻿namespace CU
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.miControl1 = new CU.MiControl();
            this.miControl2 = new CU.MiControl();
            this.miControl3 = new CU.MiControl();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(835, 61);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(194, 122);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // miControl1
            // 
            this.miControl1.Etiqueta = "Nombre";
            this.miControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miControl1.Location = new System.Drawing.Point(29, 31);
            this.miControl1.Name = "miControl1";
            this.miControl1.Requerido = true;
            this.miControl1.Size = new System.Drawing.Size(763, 72);
            this.miControl1.TabIndex = 3;
            // 
            // miControl2
            // 
            this.miControl2.Etiqueta = "Apellido";
            this.miControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miControl2.Location = new System.Drawing.Point(29, 111);
            this.miControl2.Name = "miControl2";
            this.miControl2.Requerido = true;
            this.miControl2.Size = new System.Drawing.Size(763, 72);
            this.miControl2.TabIndex = 4;
            // 
            // miControl3
            // 
            this.miControl3.Etiqueta = "DNI";
            this.miControl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miControl3.Location = new System.Drawing.Point(29, 189);
            this.miControl3.Name = "miControl3";
            this.miControl3.Requerido = true;
            this.miControl3.Size = new System.Drawing.Size(763, 72);
            this.miControl3.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 703);
            this.Controls.Add(this.miControl3);
            this.Controls.Add(this.miControl2);
            this.Controls.Add(this.miControl1);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private MiControl miControl1;
        private MiControl miControl2;
        private MiControl miControl3;
    }
}

