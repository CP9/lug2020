﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CU
{
    public partial class MiControl : UserControl
    {
        public MiControl()
        {
            InitializeComponent();
        }

        private bool requerido;

        public bool Requerido
        {
            get { return requerido; }
            set { requerido = value; }
        }


        public string Etiqueta
        {
            get { return label1.Text; }

            set {

                label1.Text = value;
            }
        }

        public bool Validar()
        {
            // true si esta todo bien
            // falso Si no esta completo 

            bool ok = true;

            if (requerido )
            {
                if (string.IsNullOrWhiteSpace(textBox1.Text))
                {
                    textBox1.BackColor = Color.Red;
                }
                else
                {
                    textBox1.BackColor = Color.White;

                }

            }


            return ok;
        }

        private void MiControl_Load(object sender, EventArgs e)
        {

        }
    }
}
