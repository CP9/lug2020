﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ejemplo_XML
{
    public partial class Form1 : Form
    {
        DataSet ds = new DataSet();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OFD.InitialDirectory = "D:\\";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                string esquema = OFD.FileName.Replace(".xml", "_esquema.xml");
                if (File.Exists(esquema))
                {
                    ds.ReadXmlSchema(esquema);
                }


                ds.ReadXml(OFD.FileName);
                 
            }
            enlazar();

        }
        void enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = ds.Tables[0];
        }
        private void button2_Click(object sender, EventArgs e)
        {
            DataRow registro = ds.Tables[0].NewRow();
            registro[0] = textBox1.Text;
            registro[1] = textBox2.Text;

            ds.Tables[0].Rows.Add(registro);
            ds.WriteXml(OFD.FileName);
            enlazar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(indice >-1)
            { 
                ds.Tables[0].Rows.RemoveAt( indice);
                indice = -1;
                ds.WriteXml(OFD.FileName);
                enlazar();
            }
        }
        int indice = -1;
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SFD.ShowDialog() == DialogResult.OK)
            {
                ds.Tables.Clear();

                ds.Tables.Add(new DataTable("ALUMNO"));

                ds.Tables["ALUMNO"].Columns.Add(new DataColumn("nombre"));

                ds.Tables["ALUMNO"].Columns.Add(new DataColumn("apellido"));

                string esquema = SFD.FileName.Replace(".xml", "_esquema.xml");

                ds.WriteXmlSchema(esquema);
                ds.WriteXml(SFD.FileName);
            }
        }
    }
}
