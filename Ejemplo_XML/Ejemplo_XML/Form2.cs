﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace Ejemplo_XML
{
    public partial class Form2 : Form
    {
        XmlNode nodo;

        XmlDocument xmldoc;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            xmldoc = new XmlDocument();

            //xmldoc.LoadXml("<archivo><nombre>aaaa</nombre></archivo>");
            xmldoc.Load(@"D:\nuevo.xml");

            nodo = xmldoc.DocumentElement;

            LeerNodo(nodo);
        }


        void LeerNodo(XmlNode unNodo)
        {
            label1.Text = unNodo.Name;
            label2.Text = unNodo.NodeType.ToString();
            label3.Text = unNodo.Value;
            label4.Text = unNodo.InnerText;
            label5.Text = unNodo.InnerXml;
            label6.Text = unNodo.HasChildNodes.ToString();
            listBox1.Items.Clear();
            foreach (XmlAttribute atributo in unNodo.Attributes)
            {
                listBox1.Items.Add(atributo.Name + " = " + atributo.Value);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(nodo.HasChildNodes)
            { 
                nodo = nodo.FirstChild;
                LeerNodo(nodo);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (nodo.HasChildNodes)
            {
                nodo = nodo.LastChild;
                LeerNodo(nodo);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(nodo.ParentNode !=null)
            { 
                nodo = nodo.ParentNode;
                LeerNodo(nodo);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(nodo.NextSibling!=null)
            { 
                nodo = nodo.NextSibling;
                LeerNodo(nodo);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if(nodo.PreviousSibling != null)
            { 
                nodo = nodo.PreviousSibling;
                LeerNodo(nodo);
            }
        }
    }
}
