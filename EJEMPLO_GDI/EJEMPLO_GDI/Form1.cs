﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EJEMPLO_GDI
{
    public partial class Form1 : Form
    {

        Bitmap bmp = new Bitmap(800,600);

        Image imagen;
        Image imagen2;

        List<Point> puntos = new List<Point>();


        enum Accion
        {
            linea = 0,
            rectangulo = 1,
            circulo = 2,
            Imagen = 3
        }

        Accion accion = Accion.linea;

        Graphics graficador;
        Graphics graficador2;

        public Form1()
        {
            InitializeComponent();
  
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            graficador = this.CreateGraphics();

            imagen2 = Image.FromHbitmap(bmp.GetHbitmap());

            graficador2 = Graphics.FromImage(imagen2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            accion = Accion.linea;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            accion = Accion.rectangulo;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            accion = Accion.circulo;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            graficador.Clear(Color.Coral);

            graficador2.Clear(Color.Coral);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            this.Text = "X: " + e.X.ToString() + " , Y: " + e.Y.ToString();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (puntos.Count == 0)
            {
                puntos.Add(new Point(e.X, e.Y));

            }
            else
            {
                puntos.Add(new Point(e.X, e.Y));


                if (accion == Accion.linea)
                {

                    graficador.DrawLine(Pens.Black, puntos[0], puntos[1]);
                    graficador2.DrawLine(Pens.Black, puntos[0], puntos[1]);
                }

                else if (accion == Accion.rectangulo)
                {

                    Rectangle rect = new Rectangle();
                    rect.X = puntos[0].X;
                    rect.Y = puntos[0].Y;

                    rect.Width = Math.Abs(puntos[0].X - puntos[1].X);
                    rect.Height = Math.Abs(puntos[0].Y - puntos[1].Y);

                    Pen lapiz = new Pen(Brushes.Blue);


                    lapiz.Width = 5;
                    graficador.FillRectangle(Brushes.Yellow, rect);
                    graficador.DrawRectangle(lapiz, rect);

                    graficador2.FillRectangle(Brushes.Yellow, rect);
                    graficador2.DrawRectangle(lapiz, rect);

                }
                else if (accion == Accion.circulo) 
                {
                    Rectangle rect = new Rectangle();
                    rect.X = puntos[0].X;
                    rect.Y = puntos[0].Y;

                    rect.Width = Math.Abs(puntos[0].X - puntos[1].X);
                    rect.Height = Math.Abs(puntos[0].Y - puntos[1].Y);
                    Pen lapiz = new Pen(Brushes.Blue);
                    lapiz.Width = 5;
                    graficador.FillEllipse(Brushes.Yellow, rect);
                    graficador.DrawEllipse(lapiz, rect);

                    graficador2.FillEllipse(Brushes.Yellow, rect);
                    graficador2.DrawEllipse(lapiz, rect);
                }
                else if(accion == Accion.Imagen)
                {
                    Rectangle rect = new Rectangle();
                    rect.X = puntos[0].X;
                    rect.Y = puntos[0].Y;

                    rect.Width = Math.Abs(puntos[0].X - puntos[1].X);
                    rect.Height = Math.Abs(puntos[0].Y - puntos[1].Y);
                    graficador.DrawImage(imagen,rect);

                    graficador2.DrawImage(imagen, rect);

                }


                puntos.Clear();
            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
            imagen = Image.FromFile("D:\\mars.jpg");
            accion = Accion.Imagen;


        }

        private void button5_Click(object sender, EventArgs e)
        {

            string Texto = "BIENVENIDOS A GDI";

            Font fuente = new Font("Arial", 20);

            graficador.DrawString(Texto,fuente,Brushes.Black,new Point(500,10));


            imagen2.Save("D:\\IMAGEN.jpg ");
        }
    }
}
