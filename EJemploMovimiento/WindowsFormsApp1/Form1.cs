﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        Thread hilo;

        Thread timer; 


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hilo = new Thread(new ThreadStart(iterar));
            hilo.Start();


          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
            SetearLabel( 100);
        }


        public void iterar()
        {
            double inicio = double.Parse(textBox2.Text);
            double fin = double.Parse(textBox1.Text);

            for (double n = inicio; n < fin; n++)
            {
                SetearLabel(n);
            }
        }

        delegate void delSetearLabel(double valor);

        void SetearLabel(double valor)
        {
            if (label1.InvokeRequired)
            {
                label1.Invoke(new delSetearLabel(SetearLabel), valor);
            }
            else
            {
                label1.Text = valor.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (hilo != null)
            {
                hilo.Abort();
                hilo = null;
            }
        }

        void MostrarHorario()
        {
            while (true)
            {
                Thread.Sleep(10000);
                SetHora();
            }
        }

        delegate void delMostrarHorario();

        void SetHora()
        {
            if (label2.InvokeRequired)
            {
                label2.Invoke(new delMostrarHorario(SetHora));
            }
            else
            {
                label2.Text = DateTime.Now.ToString();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer = new Thread(new ThreadStart(MostrarHorario));
            timer.Start();
        }

        private void Form1_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (timer != null)
            {
                timer.Abort();
            }
            if (hilo != null)
            {
                hilo.Abort();
            }
        }
    }
}
