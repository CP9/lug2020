﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;


namespace EJemploMovimiento
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Up)
            //{
            //    pictureBox1.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y - 5);

            //}
            //else if (e.KeyCode == Keys.Down)
            //{
            //    pictureBox1.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + 5);
            //}
        }

        void enlazar()
        {
            listBox1.DataSource = null;

            listBox1.DataSource = Process.GetProcesses();

            listBox1.DisplayMember = "ProcessName";

        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            enlazar();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            enlazar();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                Process p = (Process)listBox1.SelectedItem;

                label1.Text = p.ProcessName;

                label2.Text = "Esta funcionando bien? " +  p.Responding.ToString();

                try
                {
                    label3.Text = p.StartTime.ToString();
                }
                catch
                {
                    label3.Text = "No tiene permisos";
                }

                try
                {
                    label4.Text = p.TotalProcessorTime.ToString();
                }
                catch
                {
                    label4.Text = "No tiene permisos";
                }
                try
                {
                    label5.Text = p.SessionId.ToString();
                }
                catch
                {
                    label5.Text = "No tiene permisos";
                }

                try
                {
                    label6.Text = p.PriorityClass.ToString();
                }
                catch
                {
                    label6.Text = "No tiene permisos";
                }


                try
                {
                    label7.Text = p.MainWindowTitle.ToString();
                }
                catch
                {
                    label7.Text = "No tiene permisos";
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                Process p = (Process)listBox1.SelectedItem;

                p.Kill();

                enlazar();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                Process.Start(textBox1.Text);
            }
            else
            {
                Process.Start(textBox1.Text,textBox2.Text);
            }

            enlazar();
        }
    }
}
