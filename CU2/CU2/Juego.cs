﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU2
{
    class Juego
    {
        public static bool puedemover = true;

        public delegate void delOcultar();

        public event delOcultar Ocultar;

        private List<CASILLERO> casilleros = new List<CASILLERO>();

        public List<CASILLERO> CasillerosSeleccionados
        {
            get { return casilleros ; }

        }

        public void Limpiar()
        {
            casilleros.Clear();
            puedemover = true;
        }
        public void SeleccionarCasillero(CASILLERO casillero)
        {
            if (casilleros.Count > 0)
            {
                puedemover = false;
                casilleros.Add(casillero);
                if (casillero.Imagen != casilleros[0].Imagen)
                {
                    Ocultar();
                }
                else
                {
                    Limpiar();
                }
              //  casilleros.Clear();
            }
            else
            {
                casilleros.Add(casillero);
            }


        }


    }
}
