﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CU2
{
    public partial class Ficha : UserControl
    {

        public delegate void delClick (Ficha ficha);

        public event delClick FichaClick;

        public Ficha()
        {
            InitializeComponent();
        }

        private CASILLERO CASILLERO;

        public  CASILLERO Casillero
        {
            get { return CASILLERO; }
            set { CASILLERO = value; }
        }


        public void EstablecerImagen(int valor)
        {
            switch (valor)
            {
                case 1:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\1.jpg ");
                        break;
                    }

                case 2:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\2.png ");
                        break;
                    }

                case 3:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\3.png ");
                        break;
                    }
                case 4:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\4.png ");
                        break;
                    }

                default:                
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\0.jpg");
                        break;
                    }
            }
        


        }

        private void pictureBox1_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            EstablecerImagen(0);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(Juego.puedemover)
            { 
                EstablecerImagen(CASILLERO.Imagen);
                this.FichaClick(this);
            }
        }
    }
}
