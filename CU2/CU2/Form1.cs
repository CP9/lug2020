﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CU2
{
    public partial class Form1 : Form
    {
        TABLERO tablero = new TABLERO();
        Juego juego = new Juego();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
     
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is Ficha)
                {
                    ((Ficha)c).EstablecerImagen(0);

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tablero.EnviarCasillero += Tablero_EnviarCasillero;
            tablero.Repartir();

            juego.Ocultar += Juego_Ocultar;
        }

        private void Juego_Ocultar()
        {
            timer1.Enabled = true;
        }

        private void Tablero_EnviarCasillero(CASILLERO casillero)
        {
            Ficha ficha = new Ficha();
            ficha.Location = new Point((casillero.X * ficha.Width) + 5, (casillero.Y * ficha.Height)+5 ) ;
            ficha.Casillero = casillero;
            ficha.FichaClick += Ficha_FichaClick;

            this.Controls.Add(ficha);

        }

        private void Ficha_FichaClick(Ficha ficha)
        {
            juego.SeleccionarCasillero(ficha.Casillero);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (CASILLERO c in juego.CasillerosSeleccionados)
            {
                Ficha ficha = (from Ficha f in this.Controls
                               where f.Casillero == c
                               select f).First();

                ficha.EstablecerImagen(0);
            }
            timer1.Enabled = false;
            juego.Limpiar();
        }
    }
}
