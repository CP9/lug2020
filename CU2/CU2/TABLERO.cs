﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU2
{
    class TABLERO
    {
        public delegate void delEnviarCasillero(CASILLERO casillero);

        public event delEnviarCasillero EnviarCasillero;

        List<CASILLERO> casilleros = new List<CASILLERO>();

        public static Random rnd = new Random();

        public List<CASILLERO> Casilleros
        {
            get { return casilleros; }
        
        }

        public void Repartir()
        {
            List<int> imagenes = new List<int>();

            for (int i = 1; i < 5; i++)
            {
                imagenes.Add(i);
                imagenes.Add(i);
            }



            for (int fila = 0; fila < 2; fila++)
            {
                for (int col = 0; col < 4; col++)
                {
                    CASILLERO casillero = new CASILLERO();
                    casillero.X = fila;
                    casillero.Y = col;

                    //bool encontrado = false;
                    //while (!encontrado)
                    //{
                    int n = rnd.Next(0, imagenes.Count);

                    casillero.Imagen = imagenes[n];

                    imagenes.RemoveAt(n);

                    //}


                    this.EnviarCasillero(casillero);

                    Casilleros.Add(casillero);
                }
            }

         }



    }
}
