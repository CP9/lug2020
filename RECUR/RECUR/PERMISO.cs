﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RECUR
{
    class PERMISO
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public virtual bool Validar(PERMISO permiso)
        {
            return (permiso.id == this.id);
        }

        public override string ToString()
        {
            return nombre;
        }
    }
}
