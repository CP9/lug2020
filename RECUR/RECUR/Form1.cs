﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RECUR
{
    public partial class Form1 : Form
    {

        List<PERMISO> permisos = new List<PERMISO>();
        

        public Form1()
        {
            InitializeComponent();

            PERMISO p = new PERMISO();
            p.Id = 1;
            p.Nombre = "Login";
            permisos.Add(p);

            p = new PERMISO();
            p.Id = 2;
            p.Nombre = "Bitacora";
            permisos.Add(p);
            p = new PERMISO();
            p.Id = 3;
            p.Nombre = "ALTA";
            permisos.Add(p);
            p = new PERMISO();
            p.Id = 4;
            p.Nombre = "BORRADO";
            permisos.Add(p);

            p = new PERMISO();
            p.Id = 5;
            p.Nombre = "Backup";
            permisos.Add(p);

            USUARIO us = new USUARIO();
            us.Nombre = "Christian";
            listBox2.Items.Add(us);

            us = new USUARIO();
            us.Nombre = "Gastón";
            listBox2.Items.Add(us);

            us = new USUARIO();
            us.Nombre = "Carlos";
            listBox2.Items.Add(us);


        }

        private void button1_Click(object sender, EventArgs e)
        {
            // 5! =  5 x 4 x 3 x 2 x 1 
            label1.Text = CalcularFactorial(long.Parse(textBox1.Text)).ToString();

        }

        long CalcularFactorial(long numero)
        {
            if (numero > 1)
            {
                return numero * CalcularFactorial(numero -1 );
            }
            else
            {
                return 1;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarPermisos();
        }

        void EnlazarPermisos()
        {
            listBox1.DataSource = null;
            listBox1.DataSource = permisos;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GRUPO grupo = new GRUPO();

            grupo.Id = permisos.Count;

            grupo.Nombre = textBox2.Text;

            for (int i = 0; i < listBox1.SelectedItems.Count; i++)
            {
                grupo.Permisos.Add((PERMISO)listBox1.SelectedItems[i]);
            }

            permisos.Add(grupo);
            EnlazarPermisos();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count == 1 && !(listBox1.SelectedItem is GRUPO))
            {
                USUARIO us = (USUARIO)listBox2.SelectedItem;


                PERMISO per = (PERMISO)listBox1.SelectedItem;

                if (us.TieneAcceso(per))
                {
                    label2.Text = "ACCESO AUTORIZADO";

                }
                else
                {
                    label2.Text = "ACCESO DENEGADO";
                }

            }
            else
            {
                label2.Text = "Seleccione solo 1 elemento";
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            USUARIO us = (USUARIO)listBox2.SelectedItem;

            for (int i = 0; i < listBox1.SelectedItems.Count; i++)
            {
                us.Permisos.Add((PERMISO)listBox1.SelectedItems[i]);
            }

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox3.DataSource = null;
            USUARIO us = (USUARIO)listBox2.SelectedItem;
            listBox3.DataSource = us.Permisos;
        }
    }
}
