﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RECUR
{
    class GRUPO:PERMISO
    {
        private List<PERMISO> permisos = new List<PERMISO>();

        public List<PERMISO> Permisos
        {
            get { return permisos; }
            
        }

        public override bool Validar(PERMISO permiso)
        {
            bool encontrado = false;
            int indice = 0;
            while (indice < permisos.Count && !encontrado)
            {
                encontrado = permisos[indice].Validar(permiso);
                indice++;
            }
            return encontrado;
        }
        public override string ToString()
        {
            return  "[" + base.ToString() + "]" ;
        }
    }
}
