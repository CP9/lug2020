﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LUG_CLASE1
{
    public class MATERIA
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
         
        private List<Alumno> alumnos = new List<Alumno>();

        public List<Alumno> Alumnos
        {
            get { return alumnos; }
            
        }


    }
}