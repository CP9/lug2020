﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace LUG_CLASE1
{

   
    public partial class Form1 : Form
    {
        
        List<Alumno> alumnos = new List<Alumno>();

        Acceso acceso = new Acceso();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (acceso.Abrir())
            {
                label1.Text = "Conectado";
                Enlazar();
            }
            else
            {
                label1.Text = "Error de Conexión";

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            acceso.Cerrar();
            label1.Text = "Cerrado";
            alumnos.Clear();
            grilla.DataSource = null;

        }

        void Insertar()
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nom";
            nombre.Value = txtNombre.Text;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter edad = new SqlParameter();
            edad.ParameterName = "@edad";
            edad.Value = int.Parse(txtEdad.Text);
            edad.SqlDbType = SqlDbType.Int;
            parametros.Add(edad);

            string SQL = "Select ISNULL ( MAX(ID) , 0) +1 From ALUMNO";

            SqlParameter parametro = new SqlParameter("@id", 0);
            parametro.SqlDbType = SqlDbType.Int;

            parametro.Value = acceso.LeerEscalar(SQL);
            parametros.Add(parametro);

            SQL = "INSERT INTO ALUMNO (ID,NOMBRE,EDAD) values (@id, @nom, @edad)";

            int resultado = acceso.Escribir(SQL, parametros);
            if (resultado > 0)
            {
                label1.Text = "ALTA OK";
                Enlazar();
            }
            else if (resultado == -1)
            {
                label1.Text = "Error en el Alta";
            }
            else if (resultado == -2)
            {
                label1.Text = "La conexión no está abierta";
            }
        }



        void InsertarInseguro()
        {
            

            string SQL = "Select ISNULL ( MAX(ID) , 0) +1 From ALUMNO";

            
            int id = acceso.LeerEscalar(SQL);
      

            SQL = "INSERT INTO ALUMNO (ID,NOMBRE,EDAD) values (" + id + ",'" + txtNombre.Text +  "', " + txtEdad.Text + ")";

            int resultado = acceso.Escribir(SQL);
            if (resultado > 0)
            {
                label1.Text = "ALTA OK";
                Enlazar();
            }
            else if (resultado == -1)
            {
                label1.Text = "Error en el Alta";
            }
            else if (resultado == -2)
            {
                label1.Text = "La conexión no está abierta";
            }

        }


        private void button3_Click(object sender, EventArgs e)
        {
            // string SQL =  string.Format( "INSERT INTO ALUMNO ( NOMBRE,EDAD) VALUES ('{1}', {0})" , txtEdad.Text, txtNombre.Text);

            //            string SQL = "INSERT INTO ALUMNO (NOMBRE,EDAD) VALUES ('" + txtNombre.Text +  "', " + txtEdad.Text + ")";


            Insertar();
         
        }

        void Enlazar()
        {
            alumnos.Clear();

            string SQL = "Select id, nombre, edad from ALUMNO";

            SqlDataReader lector = acceso.Leer(SQL);

            while (lector.Read())
            {
                //Obteniendo los campos
                Alumno alumno = new Alumno();

                alumno.Id = int.Parse(lector["id"].ToString());
                alumno.Nombre = lector.GetString(1);

                alumno.Edad = int.Parse((lector["edad"].ToString()));

                alumnos.Add(alumno);
            }

            lector.Close();

            grilla.DataSource = null;
            grilla.DataSource = alumnos;

        }



        private void Form1_Load(object sender, EventArgs e)
        {
        
        }

        private void grilla_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Alumno alumno = (Alumno)grilla.Rows[e.RowIndex].DataBoundItem;

            txtNombre.Text = alumno.Nombre;
            txtEdad.Text = alumno.Edad.ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            acceso.IniciarTransaccion();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            acceso.ConfirmarTransaccion();
            Enlazar();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            acceso.CancelarTransaccion();
            Enlazar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nom";
            nombre.Value = txtNombre.Text;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter edad = new SqlParameter();
            edad.ParameterName = "@edad";
            edad.Value = int.Parse(txtEdad.Text);
            edad.SqlDbType = SqlDbType.Int;
            parametros.Add(edad);



            string SQL = "ALUMNO_INSERTAR";

         

            int resultado = acceso.Escribir(SQL, parametros, CommandType.StoredProcedure );
            if (resultado > 0)
            {
                label1.Text = "ALTA OK";
                Enlazar();
            }
            else if (resultado == -1)
            {
                label1.Text = "Error en el Alta";
            }
            else if (resultado == -2)
            {
                label1.Text = "La conexión no está abierta";
            }



        }
    }
}
