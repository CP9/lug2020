﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LUG_CLASE1
{
    public class Alumno
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private int edad;

        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private List<MATERIA> materias = new List<MATERIA>();

        public List<MATERIA> Materias
        {
            get { return materias; }
        }


    }
}
